package com.alh.pdfapp.controllers;

import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class PdfLogger
 */
@WebServlet("/PdfLogger")
public class PdfLogger extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger logger;
	private static FileHandler fileHandler;
	
	// Setup our Logger.
	static {
		try {
			fileHandler = new FileHandler("pdfLog.log", true);
		} catch (SecurityException | IOException e) {
			e.printStackTrace();
		}
		logger = Logger.getAnonymousLogger();
		logger.addHandler(fileHandler);
		fileHandler.setFormatter(new SimpleFormatter());
	}
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PdfLogger() {
        super();
    }

	/**
	 * doGet logs the "pdfName" parameter and current time.
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String pdfName = request.getParameter("pdfName");
		
		if (pdfName != null) {
			//System.out.println(parameters.get("pdfName"));
			//System.out.println(new java.util.Date());
			String log = (new java.util.Date()).toString() + " | " + pdfName; 
			logger.info(log);
		}
	}
}
