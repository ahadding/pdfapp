package com.alh.pdfapp.controllers;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class PdfApp
 */
@WebServlet("/PdfApp")
public class PdfApp extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String PDF_APP_VIEW = "/PdfAppView.jsp";
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PdfApp() {
        super();
    }

	/**
	 * doGet just responds with the view for the app.
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher view = request.getRequestDispatcher(PDF_APP_VIEW);
		view.forward(request, response);
	}
}
