<!DOCTYPE html>
<html lang="en">
<head>
	<title>Pdf Test App</title>
	<meta charset="utf-8">
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
	<script type="text/javascript" src="script/lib/jquery.gdocsviewer.js"></script>
	<script type="text/javascript" src="script/pdf-reader.js"></script> 
</head>
<body>
	<form class="pdf-form">
		PDF URL: <input type="text" name="pdf-url" class="url-box" />
		<input type="submit" class="read-pdf-button" value="Read it here!">
	</form>
	<section class="pdf-area">
		<a class="pdf-holder" href="#"></a>
	</section>
</body>
</html>