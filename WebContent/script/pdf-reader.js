var logUrl = 'http://localhost:8080/com.alh.webapp.combo/PdfLogger';
var urlTextFieldSelector = '.url-box';
var pdfAreaSelector = '.pdf-area';
var readPdfButtonClass = 'read-pdf-button';
var readPdfButtonSelector = '.' + readPdfButtonClass;
var pdfContainerClass = 'pdf-holder';
var pdfContainerSelector = '.' + pdfContainerClass;
var pdfForm = $('.pdf-form');

/*
 * logUrlAndTime sends the server a request to log the file name and time of
 * file view in a log file on the server, given a URL.
 */
var logUrlAndTime = function(pdfUrl) {
	logData = {
			'pdfName': pdfUrl
	};
	$.get(logUrl, logData);
};

/*
 * displayPdf displays the .pdf file located at the given URL.
 * **** Assumes the given URL actually locates a .pdf file - does not validate
 * the given URL! ****
 */
var displayPdf = function(pdfUrl) {
	pdfContainer = $(pdfContainerSelector);
	pdfContainer.remove();
	
	pdfArea = $(pdfAreaSelector);
	pdfArea.append(
			'<a class="' + pdfContainerClass + '" href="' + pdfUrl + '"></a>'
	);
	
	$(pdfContainerSelector).gdocsViewer();
};

/*
 * formSubmitHandler displays the pdf at the given url in the textbox and
 * sends the file name to the server for logging.
 */
var formSubmitHandler = function(event) {
	event.stopPropagation();
	event.preventDefault();
	pdfUrl = $(urlTextFieldSelector).val();
	displayPdf(pdfUrl);
	logUrlAndTime(pdfUrl);
};

$(document).ready(function() {
	$('.pdf-form').on('submit', formSubmitHandler);
	// Populate the field with an example, so that nobody has to hunt for one
	// to try the app.
	$(urlTextFieldSelector).val('http://www.selab.isti.cnr.it/ws-mate/example.pdf');
});